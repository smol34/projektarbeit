/*
 * network.c
 *
 *  Created on: 25.07.2020
 *      Author: sebas
 */
#include "network.h"
#include "usbd_hid.h"
#include "Krypto.h"
#include "tweetnacl.h"
#include "string.h"

// Acer Laptop as Server
#define SERVER_IP1 	192
#define SERVER_IP2	168
#define SERVER_IP3	  2
#define SERVER_IP4	176 // Wlan
//#define SERVER_IP4	117 // Ethernet

/*
// Medion Laptop as Server
#define SERVER_IP1 	192
#define SERVER_IP2	168
#define SERVER_IP3	  2
//#define SERVER_IP4	185	// Wlan
//#define SERVER_IP4 	137	// Ethernet
*/

#define CLIENT_PORT 8080
#define SERVER_PORT	8080

#define KEYBOARD_REPORTSIZE_BYTE 8
#define MOUSE_REPORTSIZE_BYTE 5

#define MLEN 9 	// 9 = KEYBOARDREPORT_SIZE im Servercode
#define crypto_secretbox_MACBYTES 16
#define CLEN (MLEN + crypto_secretbox_MACBYTES) // mlen + crypto_secretbox_MACBYTES

#define SUCCESS 1
#define FAIL	0

// Krypto
Message m;
const char* HEX_KEY = "6f763ce9f29b0bc48d662188debc897945df779f68b532440d8e6fffeb2e54bc";
const char* HEX_NONCE = "2923be84e16cd6ae529049f1f1bbe9ebb3a6db3c870c3e99";
Message nIn; // nonce received
Message nOut; // nonce used for sending
Message k; // key
// for encryption
Message m_padded;
Message c;
unsigned char cBuff[200];
Message c_trimmed;
unsigned char c_trimmedBuff[200];
// for decryption
Message c_padded;
unsigned char c_paddedBuff[200];
Message m_out;
unsigned char m_outBuff[200];
Message m_out_trimmed;
unsigned char m_out_trimmedBuff[200];
// for sending and reveiving messages
Message outMsg;
Message inMsg;
unsigned char inMsgBuff[200];
unsigned char inBuff[200];
unsigned char kBuff[200];
unsigned char nInBuff[200];
unsigned char nOutBuff[200];
unsigned char emptyForPrepad[16];

int trimmed_len;
int len;

// variable for USB handle
extern USBD_HandleTypeDef hUsbDeviceFS;

void network_lwip_init(void) {
	MX_LWIP_Init();
}

static struct udp_pcb *myUdpPcb;
extern struct netif gnetif;

int network_udp_init(void) {
	err_t udpError;

	k = hex2string(HEX_KEY, kBuff);
	nOut = hex2string(HEX_NONCE, nOutBuff);

	myUdpPcb = udp_new();

	if (myUdpPcb != NULL) {
		/* bind UDP socket on my IP adress using port 8080 */
		udpError = udp_bind(myUdpPcb, &gnetif.ip_addr, CLIENT_PORT);

		if (udpError != ERR_OK) {
			/* error handling */
			return FAIL;
		}
	} else {
		/* error handling */
		return FAIL;
	}
	return SUCCESS;
}

int udp_send_ip_to_server(void) {
	ip_addr_t serverIpAddr;
	struct pbuf *ethTxBuffer;

	IP4_ADDR(&serverIpAddr, SERVER_IP1, SERVER_IP2, SERVER_IP3, SERVER_IP4);

	ethTxBuffer = pbuf_alloc(PBUF_TRANSPORT, 4, PBUF_RAM);
	if (ethTxBuffer == NULL) {
		/* error handling */
		return FAIL;
	}

	void *p2IpAddr = &gnetif.ip_addr.addr;
	MEMCPY(ethTxBuffer->payload, p2IpAddr, sizeof(gnetif.ip_addr.addr));

	/* send UDP packet to destination port 8080 */
	err_t errCode;
	if ((errCode = udp_sendto(myUdpPcb, ethTxBuffer, &serverIpAddr, SERVER_PORT))) {
		/* error handling */
		return FAIL;
	}
	pbuf_free(ethTxBuffer);
	return SUCCESS;
}

/*
 * Der Tastatur Report hat ein zusätzliches Feld bekommen. Dies ist nötig um zu unterscheiden ob auf dem Server das C oder das
 * C++ Programm läuft. Das C Programm sendet nämlich nur ein key press event, wodurch der Mikrocontroller selbst ein key release
 * event simulieren muss, da die Taste sonst als dauerhaft gedrückt interpretiert wird. Das C++ Programm sendet sowohl key press
 * als auch key release Events.
 * Daher ist report[8] 0, wenn das C Programm auf dem Server läuft und ist 1, wenn das C++ auf dem Server läuft.
 */
unsigned char keyboardReport[KEYBOARD_REPORTSIZE_BYTE + 1];
signed char mouseReport[MOUSE_REPORTSIZE_BYTE - 1];
unsigned int messageCounter = 0;

/*
 * @brief verarbeiten des empfangenen Reports
 *
 * Die Parameter der Funktion werden beim STM32 Prototyp wie folgt beschrieben:
 * @param arg user supplied argument (udp_pcb.recv_arg)
 * @param pcb the udp_pcb which received data
 * @param p the packet buffer that was received
 * @param addr the remote IP address from which the packet was received
 * @param port the remote port from which the packet was received
 */
extern UART_HandleTypeDef huart3;
void udp_buffer_recv_data_callback(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port) {

	++messageCounter;

	inMsg = newEmpty(p->tot_len, inMsgBuff);
	MEMCPY(inMsg.data, p->payload, p->tot_len);

	// nonce
	nIn = newEmpty(crypto_secretbox_NONCEBYTES, nInBuff);
	MEMCPY(nIn.data, (inMsg.data + inMsg.size - crypto_secretbox_NONCEBYTES), crypto_secretbox_NONCEBYTES);
	nIn.size = crypto_secretbox_NONCEBYTES;
	// ciphertext
	c_trimmed = newEmpty(inMsg.size - crypto_secretbox_NONCEBYTES, c_trimmedBuff);
	MEMCPY(c_trimmed.data, inMsg.data, inMsg.size - crypto_secretbox_NONCEBYTES);
	c_trimmed.size = inMsg.size - crypto_secretbox_NONCEBYTES; // diese Zeile verändert n.data

	c_padded = zeroPadPrefix(c_trimmed, crypto_secretbox_BOXZEROBYTES, c_paddedBuff);

	m_out = newEmpty(c_trimmed.size + crypto_secretbox_ZEROBYTES, m_outBuff);

	int ret = crypto_secretbox_open(m_out.data, c_padded.data, c_padded.size, nIn.data, k.data);

	m_out_trimmed = newEmpty(m_out.size, m_out_trimmedBuff);
	m_out_trimmed.data = m_out.data + crypto_secretbox_ZEROBYTES;
	m_out_trimmed.size = m_out.size - crypto_secretbox_ZEROBYTES;
	m_out_trimmed.size = KEYBOARD_REPORTSIZE_BYTE + 1;

	/* Kopieren der empfangenen Daten in p->payload in die entsprechenden Reports. Es werden nur so viele Daten in die
	 * Reports kopiert, wie für diese auch erwartet werden.
	 */
	MEMCPY(keyboardReport, m_out_trimmed.data, KEYBOARD_REPORTSIZE_BYTE + 1);
	//MEMCPY(mouseReport, p->payload, MOUSE_REPORTSIZE_BYTE - 1);
	/* An dieser Stelle gilt es herauszufinden, ob es sich um einen Maus- oder Tastatur Report handelt. Dazu wird
	 * die gesetzte ID an erster Stelle des Reports abgefragt. Der entsprechende Report wird dann in mehreren Schritten
	 * über die USB Schnittstelle an den PC gesendet. Die USBD_HID_SendReport() Funktion ist eine Toplevel Funktion die
	 * alle weiteren Funktionen die benötigt werden aufruft.
	 */
	if (mouseReport[0] == 2) {
		USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t*) mouseReport, MOUSE_REPORTSIZE_BYTE - 1);
	} else {
		USBD_HID_SendReport(&hUsbDeviceFS, keyboardReport, KEYBOARD_REPORTSIZE_BYTE);
	}

	// Wir unternehmen erst wieder etwas, wenn der USB handler den Report verarbeitet hat und sich im Leerlauf befindet.
	USBD_HID_HandleTypeDef *hhid = (USBD_HID_HandleTypeDef*) hUsbDeviceFS.pClassData;
	while (hhid->state != HID_IDLE) {
	}

	/* Wenn es sich um einen Tastatur Report handelt, der vom C Programm empfangen wurde, müssen wir anschließend einen
	 * leeren Report mit Report ID = 1 senden, um ein key release Event zu simulieren.
	 */
	if (keyboardReport[0] == 1 && keyboardReport[8] == 0) {
		for (int i = 1; i < KEYBOARD_REPORTSIZE_BYTE; ++i) {
			keyboardReport[i] = 0;
		}
		USBD_HID_SendReport(&hUsbDeviceFS, keyboardReport, 8);
	}

	/* Um einen Pufferüberlauf zu vermeiden, muss der pbuf geleert werden, da dieser alle empfangenen Reports speichert und
	 * nur endlich groß ist.
	 */
	pbuf_free(p);
}

void udp_recv_data_init(void) {
	udp_recv(myUdpPcb, udp_buffer_recv_data_callback, NULL);
}
