/*
 * Krypto.c
 *
 *  Created on: 21.07.2021
 *      Author: sebas
 */
#include "Krypto.h"
#include "opt.h"
#include "string.h"

// function for creating a message
Message fromStr(const char* str) {
	Message m = { (unsigned char*)str, strlen(str) };
	return m;
}

// function for creating an empty message
Message newEmpty(int len, unsigned char * buff) {
	Message m = { (unsigned char*) buff, len };
	memset(m.data, 0, len);
	return m;
}

// needed padding for encrypting and decrypting a message with the nacl C interface
Message zeroPadPrefix(Message m, int len, unsigned char * buff) {
	Message m2 = newEmpty(m.size + len, buff);
	memcpy(m2.data + len, m.data, m.size);
	return m2;
}

// random function to generate a random nonce +		n	{data=0x00007ff64ad5eb30 "" size=24 }	Message
void randombytes(unsigned char * dst, unsigned long long count) {
	long long t = GetTicks();
	for (unsigned long long i = 0; i < count; ++i) {
		*(dst + i) = (unsigned char) t;
		t = GetTicks();
	}
}


// Convert string of chars to its NULL-terminated representative string of hex
// numbers. The returned pointer must be freed by the caller.
const char* string2hex(Message str, int capital)
{
	int hexstr_size = str.size * 2;
	char* hexstr = (char *)calloc(hexstr_size + 1, 1); // +1 for terminating NULL

	const int a = capital ? 'A' - 1 : 'a' - 1;

	for (int i = 0, c = str.data[0] & 0xFF; i < hexstr_size; c = str.data[i / 2] & 0xFF)
	{
		hexstr[i++] = c > 0x9F ? (c / 16 - 9) | a : c / 16 | '0';
		hexstr[i++] = (c & 0xF) > 9 ? (c % 16 - 9) | a : c % 16 | '0';
	}

	return hexstr;
}

// Convert string of hex numbers to its equivalent char-stream
Message hex2string(const char* hexstr, unsigned char *buff)
{
	int size = strlen(hexstr);
	Message str = newEmpty((size + 1) / 2, buff);

	for (int i = 0, j = 0; i < str.size; i++, j++)
	{
		str.data[i] = (hexstr[j] & '@' ? hexstr[j] + 9 : hexstr[j]) << 4, j++;
		str.data[i] |= (hexstr[j] & '@' ? hexstr[j] + 9 : hexstr[j]) & 0xF;
	}
	return str;
}
