/*
 * Krypto.h Most Functions used for the Cryoptographie are adoptet from https://gist.github.com/augustoroman/9066d893ab7604dc73fc38e3c860612d
 *
 *  Created on: 21.07.2021
 *      Author: sebas
 */

#ifndef INC_KRYPTO_H_
#define INC_KRYPTO_H_

#include <string.h>
#include <sys/types.h>

// tpydef for a message or string
typedef struct {
	unsigned char* data;
	int size;
} Message;

// function for creating a message
Message fromStr(const char* str);

// function for creating an empty message
Message newEmpty(int len, unsigned char * buff);

// needed padding for encrypting and decrypting a message with the nacl C interface
Message zeroPadPrefix(Message m, int len, unsigned char * buff);

// Convert string of chars to its NULL-terminated representative string of hex
// numbers. The returned pointer must be freed by the caller.
const char* string2hex(Message str, int capital);

// Convert string of hex numbers to its equivalent char-stream
Message hex2string(const char* hexstr, unsigned char *buff);

#endif /* INC_KRYPTO_H_ */
