/*
 * network.h
 *
 *  Created on: 25.07.2020
 *      Author: sebas
 */

#ifndef INC_NETWORK_H_
#define INC_NETWORK_H_

#include "lwip.h"

void network_lwip_init(void);
int network_udp_init(void);
int udp_send_ip_to_server(void);
void udp_buffer_recv_data_callback(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port);
void udp_recv_data_init(void);

#endif /* INC_NETWORK_H_ */
