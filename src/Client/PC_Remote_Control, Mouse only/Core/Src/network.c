/*
 * network.c
 *
 *  Created on: 25.07.2020
 *      Author: sebas
 */
#include "network.h"
#include "usbd_hid.h"
//#include "tcp.h"

#define SERVER_IP1	192
#define SERVER_IP2	168
#define SERVER_IP3	2
#define SERVER_IP4	176

#define CLIENT_PORT 8080
#define SERVER_PORT	8080

#define KEYBOARD_REPORTSIZE_BYTE 8

#define SUCCESS 1
#define FAIL	0

// variable for USB report
extern USBD_HandleTypeDef hUsbDeviceFS;

/*
 report[0] = 1; // reportID
 report[1] = 0; // modifier
 report[2] = 0; // OEM
 report[3] = 0; // keycode data
 report[4] = 0; // keycode data
 report[5] = 0; // keycode data
 report[6] = 0; // keycode data
 report[7] = 0; // keycode data

 report[8] = 0/1 // set by sending program
 */

void network_lwip_init(void) {
	MX_LWIP_Init();
}

static struct udp_pcb *myUdpPcb;
extern struct netif gnetif;

int network_udp_init(void) {
	err_t udpError;

	myUdpPcb = udp_new();

	if (myUdpPcb != NULL) {
		/* bind UDP socket on my IP adress using port 8080 */
		udpError = udp_bind(myUdpPcb, &gnetif.ip_addr, CLIENT_PORT);

		if (udpError != ERR_OK) {
			/* error handling */
			return FAIL;
		}
	} else {
		/* error handling */
		return FAIL;
	}
	return SUCCESS;
}

int udp_send_init(void) {
	ip_addr_t serverIpAddr;
	struct pbuf *ethTxBuffer;

	IP4_ADDR(&serverIpAddr, SERVER_IP1, SERVER_IP2, SERVER_IP3, SERVER_IP4);

	ethTxBuffer = pbuf_alloc(PBUF_TRANSPORT, 4, PBUF_RAM);
	if (ethTxBuffer == NULL) {
		/* error handling */
		return FAIL;
	}

	void *p2IpAddr = &gnetif.ip_addr.addr;
	MEMCPY(ethTxBuffer->payload, p2IpAddr, sizeof(gnetif.ip_addr.addr));

	/* send UDP packet to destination port 8080 */
	err_t errCode;
	if ((errCode = udp_sendto(myUdpPcb, ethTxBuffer, &serverIpAddr, SERVER_PORT))) {
		/* error handling */
		return FAIL;
	}
	pbuf_free(ethTxBuffer);
	return SUCCESS;
}

// report[8] defines is 0 when the C program is the src program and 1 when the C++ program is the src program
unsigned char report[KEYBOARD_REPORTSIZE_BYTE + 1];
void udp_buffer_recv_data_callback(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port) {
	MEMCPY(report, p->payload, p->len);
	USBD_HID_SendReport(&hUsbDeviceFS, report, 8);
	USBD_HID_HandleTypeDef *hhid = (USBD_HID_HandleTypeDef*) hUsbDeviceFS.pClassData;
	while (hhid->state != HID_IDLE) {
	}
	if (report[8] == 0) {
		for (int i = 1; i < KEYBOARD_REPORTSIZE_BYTE; ++i) {
			report[i] = 0;
		}
		USBD_HID_SendReport(&hUsbDeviceFS, report, 8);
	}
	pbuf_free(p);
}

void udp_recv_data_init(void) {
	udp_recv(myUdpPcb, udp_buffer_recv_data_callback, NULL);
}

/*-----------------------------------------------------------------------------------------------------------*/
//static struct tcp_pcb *myTcpPcb;
//
//void my_tcp_init(void) {
//	err_t errCode = 0;
//	ip_addr_t serverIpAddr;
//
//	myTcpPcb = tcp_new();
//
//	if (myTcpPcb != NULL) {
//		/* bind TCP socket on my IP adress using port 8080 */
//		errCode = tcp_bind(myTcpPcb, &gnetif.ip_addr, CLIENT_PORT);
//		if (errCode != ERR_OK) {
//			/* error handling */
//		}
//	} else {
//		/* error handling */
//	}
//
//	IP4_ADDR(&serverIpAddr, 192, 168, 2, 176);
//	if ((errCode = tcp_connect(myTcpPcb, &serverIpAddr, SERVER_PORT, NULL))) {
//		/* error handling */
//	}
//}
//
//void tcp_send_data(void) {
//	err_t errCode = 0;
//	struct pbuf *ethTxBuffer;
//
//	ethTxBuffer = pbuf_alloc(PBUF_TRANSPORT, 8, PBUF_RAM);
//	if (ethTxBuffer == NULL) {
//		/* error handling */
//	}
//	void *p2IpAddr = &gnetif.ip_addr.addr;
//	memcpy(ethTxBuffer->payload, p2IpAddr, sizeof(gnetif.ip_addr.addr));
//
//	if ((errCode = tcp_write(myTcpPcb, ethTxBuffer->payload, sizeof(gnetif.ip_addr.addr), 0))) {
//		/* error handling */
//	}
//	/* send TCP packet to destination port 8080 */
//	if ((errCode = tcp_output(myTcpPcb))) {
//		/* error handling */
//	}
//	tcp_close(myTcpPcb);
//	pbuf_free(ethTxBuffer);
//}
