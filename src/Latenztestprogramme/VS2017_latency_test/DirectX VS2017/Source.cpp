/* This Project is adoptet from https://github.com/Pindrought/DirectX-11-Engine-VS2017/tree/Tutorial_2 
 * 	and only adaptet by me for usage.
 */
#include "Engine.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow) {

	unsigned char report[8];
	report[0] = 1; // reportID
	report[1] = 0; // modifier
	report[2] = 0; // OEM
	report[3] = 0; // keycode data
	report[4] = 0; // keycode data
	report[5] = 0; // keycode data
	report[6] = 0; // keycode data
	report[7] = 0; // keycode data

	Engine engine;
	engine.Initialize(hInstance, "PC_Remote_Control_Server", "MyWindowClass", 800, 600);
	while (engine.ProcessMessages() == true)
	{
		engine.Update();
	}
	return 0;
}