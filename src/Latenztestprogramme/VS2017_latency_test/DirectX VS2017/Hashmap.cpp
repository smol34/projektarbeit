#include "Hashmap.h"
#include <map>
#include <Windows.h>
#include <WinUser.h>
//#include <iostream> 
//#include <iterator> 

//  Key ist the DirectInput Keyocde and the value is the USB HID Keycode
std::map<int, int> KeycodeMap;	// Empty map container for keys without modifiers
std::map<int, int> KeycodeModMap; // Empty map container for modifier keys
std::map<int, int>::iterator it;

void Hashmap::FillHashmap_Mod()
{
	KeycodeModMap.insert(std::pair<int, int>(VK_SHIFT, 0x02));
	KeycodeModMap.insert(std::pair<int, int>(VK_CONTROL, 0x01));
	KeycodeModMap.insert(std::pair<int, int>(0xb8, 0x04)); // left alt
	KeycodeModMap.insert(std::pair<int, int>(VK_MENU, 0x40));
}

// insert all Keycodes
void Hashmap::FillHashmap_Keys()
{
	// letters
	KeycodeMap.insert(std::pair<int, int>(89, 29));
	KeycodeMap.insert(std::pair<int, int>(90, 28));
	for (int i = 0; i < 25; ++i)
	{
		KeycodeMap.insert(std::pair<int, int>(i + 65, i + 4));
	}
	// numbers
	KeycodeMap.insert(std::pair<int, int>(48, 39));
	for (int i = 0; i < 9; ++i)
	{
		KeycodeMap.insert(std::pair<int, int>(i + 49, i + 30));
	}
}

// if return is not less than 0 a key was found, else a mod was found
int Hashmap::FindHIDKey(int InputKey)
{
	int mod = FindHIDMod(InputKey);
	if (mod != notFound) 
	{
		return mod - modFound;
	}
	else 
	{
		it = KeycodeMap.find(InputKey);
		if (it == KeycodeMap.end())
		{
			return notFound;
		}
		return it->second;
	}
	return notFound;
}

// if return is not -99 a modifier was found
int Hashmap::FindHIDMod(int InputKey)
{
	it = KeycodeModMap.find(InputKey);
	if (it == KeycodeModMap.end())
	{
		return notFound;
	}
	return it->second;
}