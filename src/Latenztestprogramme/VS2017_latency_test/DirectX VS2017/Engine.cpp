#include <string>
#include "Engine.h"
#include "Network.h"
#include "ErrorLogger.h"

void Engine::StartNetwork() 
{
	receive_udp_datagram();
	udp_send_init();
}

void Engine::HashmapInit()
{
	Hashmap.FillHashmap_Keys();
	Hashmap.FillHashmap_Mod();
}

bool Engine::Initialize(HINSTANCE hInstance, std::string window_title, std::string window_class, int width, int height, bool useNetwork)
{
	this->HashmapInit();
	this->useNetwork = useNetwork;
	if (useNetwork) 
	{
		this->StartNetwork();
	}
	return this->render_window.Initialize(this, hInstance, window_title, window_class, width, height);
}

bool Engine::ProcessMessages()
{
	return this->render_window.ProcessMessages();
}

/*
	Tasatureingaben die weitere Überlegungen erfordern, und erstmal nicht bearbeitet werden: - CAPS lock
																						     - NUM lock																		
*/

void Engine::Update()
{
	while (!keyboard.KeyBufferIsEmpty())
	{
		ErrorLogger::Log("");
		KeyboardEvent kbe = keyboard.ReadKey();
		unsigned char keycode = kbe.GetKeyCode();

		int KeyAsHID = Hashmap.FindHIDKey(keycode);

		std::string keyEvent;
		std::string HIDKey;

		// print convertet HID modifier, if strg + alt is pressed and one of the keys is released, mod will switch to zero
		if (kbe.IsPress()) 
		{
			keyEvent = "\nKey Press\n";
			if (KeyAsHID < 0) 
			{
				HIDKey = "HIDModifier: ";
				report[1] = KeyAsHID + modFound;
			}
			else
			{
				HIDKey = "HIDKey: ";
				for (int i = 3; i <= 7; ++i) {
					if (report[i] == 0) {
						report[i] = KeyAsHID;
						break;
					}
				}
			}
		}
		if (kbe.IsRelease())
		{
			keyEvent = "\nKey Release\n";
			if (KeyAsHID < 0)
			{
				HIDKey = "HIDModifier: ";
				report[1] = 0;
			}
			else 
			{
				HIDKey = "HIDKey: ";
				for (int i = 3; i <= 7; ++i) {
					if (report[i] == KeyAsHID) {
						report[i] = 0;
					}
				}
			}
		}
		OutputDebugStringA(keyEvent.c_str());

		// print keycode macro
		std::string outmsg = "DirectInput Keycode as Macro: ";
		outmsg += keycode;
		outmsg += "\n";
		OutputDebugStringA(outmsg.c_str());

		// print keycode as int
		unsigned int keyCodeInt = kbe.GetKeyCode();
		std::string outmsgChar = "DirectInput Keycode as int: ";
		outmsgChar += std::to_string(keyCodeInt);
		outmsgChar += "\n";
		OutputDebugStringA(outmsgChar.c_str());

		// print HID Key
		HIDKey += std::to_string(KeyAsHID);
		HIDKey += "\n";
		OutputDebugStringA(HIDKey.c_str());
	}
}

