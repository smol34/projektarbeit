#pragma once

class KeyboardEvent
{
public:
	enum EventType
	{
		Press,
		Release,
		Invalid
	};

	KeyboardEvent();												// invalid event
	KeyboardEvent(const EventType type, const unsigned char key);	// event type and key
	bool IsPress() const;											// return event  press
	bool IsRelease() const;											// return event  release
	bool IsValid() const;											// return event  invalid
	unsigned char GetKeyCode() const;								// return keyCode

private:
	EventType type;
	unsigned char key;
};