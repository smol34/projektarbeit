/* This file is Coded in Pure C code, nothing in here ist coded in C++, expect from the
 * #pragma once compiler directive to make sure, this header is only included once in the build
 */
#pragma once

#define BUFFERSIZE 4

void receive_udp_datagram(void);
void udp_send_init(void);
void send_udp_datagram(char* input);
void send_udp_datagram_USB(unsigned char keyboardReport[8]);
void udp_close_socket(void);

