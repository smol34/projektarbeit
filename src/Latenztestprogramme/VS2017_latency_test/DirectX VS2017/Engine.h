#pragma once
#include "WindowContainer.h"
#include "Hashmap.h"

class Engine : WindowContainer
{
public:
	unsigned char report[8];
	void StartNetwork();
	Hashmap Hashmap;
	bool Initialize(HINSTANCE hInstance, std::string window_title, std::string window_class, int width, int height, bool useNetwork = false);
	bool ProcessMessages();
	void Update();
private:
	bool useNetwork;
	void HashmapInit();
};