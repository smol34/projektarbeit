#pragma once

constexpr auto notFound = (-999);
constexpr auto modFound = (100);

class Hashmap
{
public:
	void FillHashmap_Mod();
	void FillHashmap_Keys();
	int FindHIDKey(int InputKey);
private:
	int FindHIDMod(int InputKey);
};