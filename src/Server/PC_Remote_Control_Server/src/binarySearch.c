/* Adaptet from https://www.geeksforgeeks.org/binary-search/ */

#include "../inc/binarySearch.h"

/* A recursive binary search function.
 * It returns location of x in given array arr[l..r][] if present, otherwise -1.
 */
int idx = 0;
int binarySearch(int arr[][4], int l, int r, int x)
{
    if (r >= l) {
        int mid = l + (r - l) / 2;

        // If the element is present at the middle itself
        if (arr[mid][0] == x) {
	    idx = mid;
            return mid;
        }

        // If element is smaller than mid, then it can only be present in left subarray
        if (arr[mid][0] > x)
            return binarySearch(arr, l, mid - 1, x);

        // Else the element can only be present in right subarray
        return binarySearch(arr, mid + 1, r, x);
    }

    // We reach here when element is not
    // present in array
    return -1;
}
