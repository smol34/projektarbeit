/*
 * ascii_to_usb.c
 *
 *  Created on: 24.08.2020
 *      Author: sebas
 */
#include <stdio.h>

#include "../inc/ascii_to_usb.h"
#include "../inc/usb_hid_keys_german.h"
#include "../inc/binarySearch.h"

/* directInput von directX: windows Fenster eingaben auslesen */
/* Latenzzeit von input und Ausgabe am Ziel, key press latency messurement */


/*
 * Es handelt sich hier um das Auslesen der ASCII Zeichen, nicht um direkte Eingabe.
 * Daher sind nur eingaben m�glich die einen ASCII wert haben.
 * Nicht m�glich: - F1 bis F9 sowie das ganze in Kombination mit Fn
 * 		  - Eingaben mit STRG
 * 		  - einzelne Eingaben von Modifizieren wie SHIFT, STRG, ALT, ALTGR, Fn
 * 		  - Eingaben wie Scrollen mit den Tasten
 * 		  - Eingabe von Sondertasten wie Windows Taste
 * 		  - Anpassen der Lautst�rke oder Bildschirmhelligkeit mit Tasten
 * 		  - Eingaben mit � und `sind Fehlerhaft, da es beim USB report nur ein Modifiezierer Feld gibt,
 * 		    dass entweder gesetzt oder nicht gesetzt ist. Daher werden diese Eingaben nicht bearbeitet.
 */

#define OFFSET_LETTERS_UPPERCASE 61
#define OFFSET_LETTERS_LOWERCASE 93
#define OFFSET_NUMBERS 19
#define FIRST_D_SIZE 57
#define SEC_D_SIZE 4

/*
 * lookUpTable an Stelle 0 sind noch zu behandelten ASCII Zeichen von klein nach gro� sortiert.
 * lookUpTable an Stelle 1 sind die zum ASCII Zeichen passenden USB codes.
 * lookUpTable an Stelle 2 ist der zweite zum ASCII Zeichen passenden USB code.
 * lookUpTable an Stelle 3 ist der dazugeh�rigen modifizierer, wenn dieser n�tig ist.
 * ASCII Codes f�r erweitere Zeichen wurden von hier �bernommen: http://www.asciitable.com/
 */
int lookUpTable[FIRST_D_SIZE][SEC_D_SIZE] = {
						{3, DE_RET, 0, 0},
						{8, DE_BS, 0, 0},
						{9, DE_TAB, 0, 0},
						{13, DE_RET, 0, 0},
						{27, DE_ESC, 0, 0},
						{32, DE_SPACE, 0, 0},
						{33, DE_1, 0, MOD_LSHIFT},
						{34, DE_2, 0, MOD_LSHIFT},
						{35, DE_HASH, 0, 0},
						{36, DE_4, 0, MOD_LSHIFT},
						{37, DE_5, 0, MOD_LSHIFT},
						{38, DE_6, 0, MOD_LSHIFT},
						{39, DE_HASH, 0, MOD_LSHIFT},
						{40, DE_8, 0, MOD_LSHIFT},
						{41, DE_9, 0, MOD_LSHIFT},
						{42, DE_PLUS, 0, MOD_LSHIFT},
						{43, DE_PLUS, 0, 0},
						{44, DE_COMM, 0, 0},
						{45, DE_MINS, 0, 0},
						{46, DE_DOT, 0, 0},
						{47, DE_7, 0, MOD_LSHIFT},
						{58, DE_DOT, 0, MOD_LSHIFT},
						{59, DE_COMM, 0, MOD_LSHIFT},
						{60, DE_LABK, 0, 0},
						{61, DE_0, 0, MOD_LSHIFT},
						{62, DE_LABK, 0, MOD_LSHIFT},
						{63, DE_SS, 0, MOD_LSHIFT},
						{64, DE_Q, 0, MOD_RALT},
						{91, DE_8, 0, MOD_RALT},
						{92, DE_SS, 0, MOD_RALT},
						{93, DE_9, 0, MOD_RALT},
						{94, DE_CIRC, 0, 0},
						{95, DE_MINS, 0, MOD_LSHIFT},
						{96, DE_ACUT, 0, MOD_LSHIFT},
						{123, DE_7, 0, MOD_RALT},
						{124, DE_LABK, 0, MOD_RALT},
						{125, DE_0, 0, MOD_RALT},
						{126, DE_PLUS, 0, MOD_RALT},
						{127, DE_BS, 0, 0},
						{129, DE_UDIA, 0, 0},
						{131, DE_A, DE_CIRC, 0 },
						{132, DE_ADIA, 0, 0},
						{136, DE_E, DE_CIRC, 0 },
						{140, DE_I, DE_CIRC, 0 },
						{141, DE_I, DE_ACUT, MOD_LSHIFT },
						{142, DE_ADIA, 0, MOD_LSHIFT},
						{147, DE_O, DE_CIRC, 0 },
						{148, DE_ODIA, 0, 0},
						{150, DE_U, DE_CIRC, 0 },
						{153, DE_ODIA, 0, MOD_LSHIFT},
						{154, DE_UDIA, 0, MOD_LSHIFT},
						{230, DE_M, 0, MOD_RALT},
						{239, DE_ACUT, 0, 0},
						{245, DE_3, 0, MOD_LSHIFT},
						{248, DE_CIRC, 0, MOD_LSHIFT},
						{252, DE_3, 0, MOD_RALT},
						{253, DE_2, 0, MOD_RALT}
						};


void ascii_to_usbCode(char getch, unsigned char report[8]) {
  report[1] = 0;
  report[3] = 0;
  report[4] = 0;
  // Gro�buchstaben
  if (getch >= 65 && getch <= 90) {
    report[1] = MOD_LSHIFT;
    report[3] = getch - OFFSET_LETTERS_UPPERCASE;
//    printf("key struck: %c\n", getch);
    return;
  }
  // Kleinbuchstaben
  if (getch >= 97 && getch <= 122) {
    if (getch == 121) {
      report[3] = DE_Y;
      return;
    }
    if (getch == 122) {
      report[3] = DE_Z;
      return;
    }
    report[3] = getch - OFFSET_LETTERS_LOWERCASE;
    return;
  }
  // Zahlen
  if (getch >= 48 && getch <= 57) {
    if (getch == 48) {
      report[3] = DE_0;
      return;
    } else {
      report[3] = getch - OFFSET_NUMBERS;
      return;
    }
  }
   
  // durchsuche lookUpTable
   extern int idx;
   int found = binarySearch(lookUpTable, 0, FIRST_D_SIZE, getch);
   if (found != -1) {
     report[1] = lookUpTable[idx][3];	// modifizierer
     report[3] = lookUpTable[idx][1];	// USB Code 1
     report[4] = lookUpTable[idx][2];	// USB Code 2
   } else {
     report[1] = 0;	// modifizierer
     report[3] = 0;	// USB Code 1
     report[4] = 0;	// USB Code 2
   }


//  for (int i = 0; i < FIRST_D_SIZE; ++i) {
//    if (lookUpTable[i][0] == getch) {
//      report[1] = lookUpTable[i][3];	// modifizierer
//      report[3] = lookUpTable[i][1];	// USB Code 1
//      report[4] = lookUpTable[i][2];	// USB Code 2
//      return;
//    }
//  }
}
