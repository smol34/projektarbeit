/*
 * network.c
 *
 *  Created on: 12.07.2020
 *      Author: sebas
 */

#if defined WIN32
#include <winsock.h>
#else
#define closesocket close
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#endif

#include <stdio.h>
#include "../inc/network.h"

#define SERVERPORT 8080
#define CLIENTPORT 8080

int clearWinSock(void) {
#if defined WIN32
  int res = WSACleanup();
  return res;
#endif
}

unsigned char buffer[BUFFERSIZE]; //buffer where the received datagram ist stored, unsigned oder uint8_t wichtig
void receive_udp_datagram(void) {
  printf("____Receive client IP adress____\n");
// Step 1: Initialization of Winsock
#if defined WIN32
  WSADATA winSockData;
  int wsaStartup;
  wsaStartup = WSAStartup(MAKEWORD(2, 2), &winSockData);
  if (wsaStartup != 0) {
    printf("WSAStartup failed\n");
  } else {
    printf("WSAStartup Success\n");
  }
#endif

  // Step 2: Fill the udpClient (SOCKET ADDRESS) structure
  struct sockaddr_in udpClient;
  udpClient.sin_family = AF_INET;
  udpClient.sin_addr.s_addr = INADDR_ANY; // Client IP
  udpClient.sin_port = htons(SERVERPORT); // destination port where the datagram is received

  // Step 3: Create a socket for this Server
  SOCKET udpSocketServer;
  udpSocketServer = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);    //UDP Socket
  if (udpSocketServer == SOCKET_ERROR) {
    printf("Socket creation failed -> %d\n", WSAGetLastError());
  } else {
    printf("Socket successfully created\n");
  }

  // Step 4: Bind the server socket
  int bindSocket = bind(udpSocketServer, (SOCKADDR*) &udpClient, sizeof(udpClient));
  if (bindSocket == SOCKET_ERROR) {
    printf("Binding socket failed -> %d\n", WSAGetLastError());
  } else {
    printf("Socket successfully binded\n\n");
  }

  // Step 5: Receive data from client
  int recvData;
  recvData = recvfrom(udpSocketServer, (char*) buffer, sizeof(buffer), MSG_PEEK, NULL, NULL);
  if (recvData == SOCKET_ERROR) {
    printf("Receiving datagram failed -> %d\n", WSAGetLastError());
  } else {
    printf("Received data -> %02x\n", buffer[0]);
    printf("Received data -> %02x\n", buffer[1]);
    printf("Received data -> %02x\n", buffer[2]);
    printf("Received data -> %02x\n\n", buffer[3]);
  }

  // Step 6: Close socket
  int closeSocket;
  closeSocket = closesocket(udpSocketServer);
  if (closeSocket == SOCKET_ERROR) {
    printf("Closing socket failed -> %d\n", WSAGetLastError());
  } else {
    printf("Socket successfully closed\n");
  }

  // Step 7: WSACleanup for terminating from DLL
  int wsaCleanup;
  wsaCleanup = clearWinSock();
  if (wsaCleanup == SOCKET_ERROR) {
    printf("WSACleanup failed -> %d\n", WSAGetLastError());
  } else {
    printf("WSACleanup Success\n");
  }
}

struct sockaddr_in udpClient;
SOCKET udpSocketServer;

void udp_send_init(void) {
  printf("\n____Send UPD datagram to client____\n");
  // Step 1: Initialization of Winsock
#if defined WIN32
  WSADATA winSockData;
  int wsaStartup;
  wsaStartup = WSAStartup(MAKEWORD(2, 2), &winSockData);
  if (wsaStartup != 0) {
    printf("WSAStartup failed\n");
  } else {
    printf("WSAStartup Success\n");
  }
#endif

  // Step 2: Fill the udpServer (SOCKET ADDRESS) structure

  // Step 2.1: Get the correct format for the client IP adress to fill it into the udpClient Structure
  char bufferForIpAddr[12 + 3 + 1];

  sprintf(bufferForIpAddr, "%d.%d.%d.%d", buffer[0], buffer[1], buffer[2], buffer[3]);

  // Step 2.2: Fill the structure
  udpClient.sin_family = AF_INET;
  udpClient.sin_addr.s_addr = inet_addr(bufferForIpAddr); // Client IP
  udpClient.sin_port = htons(CLIENTPORT); // Client port where the datagram gets send to

  // Step 3: Create a socket for this Server
  udpSocketServer = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);    //UDP Socket
  if (udpSocketServer == SOCKET_ERROR) {
    printf("Socket creation failed -> %d\n", WSAGetLastError());
  } else {
    printf("Socket successfully created\n");
  }

  printf("bufferForIpAddr %s\n", bufferForIpAddr);
}

void send_udp_datagram(char *input) {
  // Step 4: send UDP datagram to client
  int sendData;
  sendData = sendto(udpSocketServer, input, strlen(input) + 1, MSG_DONTROUTE, (SOCKADDR*) &udpClient, sizeof(udpClient));
  if (sendData == SOCKET_ERROR) {
    printf("Sending datagram failed -> %d\n", WSAGetLastError());
  } else {
    printf("Datagram successfully sended\n");
  }
}

void udp_close_socket(void) {
  // Step 5: Close socket
  int closeSocket;
  closeSocket = closesocket(udpSocketServer);
  if (closeSocket == SOCKET_ERROR) {
    printf("Closing socket failed -> %d\n", WSAGetLastError());
  } else {
    printf("Socket successfully closed\n");
  }

  // Step 6: WSACleanup for terminating from DLL
  int wsaCleanup;
  wsaCleanup = clearWinSock();
  if (wsaCleanup == SOCKET_ERROR) {
    printf("WSACleanup failed -> %d\n", WSAGetLastError());
  } else {
    printf("WSACleanup Success\n");
  }
}

void send_udp_datagram_USB(unsigned char keyboardReport[USB_REPORT_SIZE + 1]) {
  // Step 4: send UDP datagram to client
  int sendData;
  // ACHTUNG! Daten sind unsigned werden aber als Signed verschickt, m�ssen daher beim Empf�nger richtig interpretiert werden
  sendData = sendto(udpSocketServer, (char*) keyboardReport, sizeof(keyboardReport[0]) * (USB_REPORT_SIZE + 1), MSG_DONTROUTE, (SOCKADDR*) &udpClient,
		    sizeof(udpClient));
  if (sendData == SOCKET_ERROR) {
    printf("Sending datagram failed -> %d\n", WSAGetLastError());
  } else {
    printf("Datagram successfully sended\n");
  }
}

