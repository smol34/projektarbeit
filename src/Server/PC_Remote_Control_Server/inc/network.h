/*
 * network.h
 *
 *  Created on: 12.07.2020
 *      Author: sebas
 */

#ifndef INC_NETWORK_H_
#define INC_NETWORK_H_

#define BUFFERSIZE 4
#define USB_REPORT_SIZE 8

void receive_udp_datagram(void);
void udp_send_init(void);
void send_udp_datagram(char* input);
void send_udp_datagram_USB(unsigned char keyboardReport[8]);
void udp_close_socket(void);

#endif /* INC_NETWORK_H_ */
