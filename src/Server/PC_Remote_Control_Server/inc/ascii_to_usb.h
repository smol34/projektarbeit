/*
 * ascii_to_usb.h
 *
 *  Created on: 24.08.2020
 *      Author: sebas
 */

#ifndef INC_ASCII_TO_USB_H_
#define INC_ASCII_TO_USB_H_

void ascii_to_usbCode(char getch, unsigned char report[8]);

#endif /* INC_ASCII_TO_USB_H_ */
