/*
 * binarySearch.h
 *
 *  Created on: 14.10.2020
 *      Author: sebas
 */

#ifndef INC_BINARYSEARCH_H_
#define INC_BINARYSEARCH_H_

int binarySearch(int arr[][4], int l, int r, int x);

#endif /* INC_BINARYSEARCH_H_ */
