/*
 * usb_hid_keys.h
 *
 *  Created on: 24.08.2020
 *      Author: sebas
 *
 *  Adaptet from https://github.com/qmk/qmk_firmware/blob/master/quantum/keymap_extras/keymap_german.h and
 *  modified with help from http://iks.cs.ovgu.de/~elkner/keyboard/mac/scancodes_MBP.html
 */

#ifndef INC_USB_HID_KEYS_GERMAN_H_
#define INC_USB_HID_KEYS_GERMAN_H_

#define MOD_LCTRL  0x01
#define MOD_LSHIFT 0x02
#define MOD_LALT   0x04
#define MOD_LMETA  0x08
#define MOD_RCTRL  0x10
#define MOD_RSHIFT 0x20
#define MOD_RALT   0x40
#define MOD_RMETA  0x80

/*
 * Normal Symbols
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐
 * │ ^ │ 1 │ 2 │ 3 │ 4 │ 5 │ 6 │ 7 │ 8 │ 9 │ 0 │ ß │ ´ │       │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤
 * │     │ Q │ W │ E │ R │ T │ Z │ U │ I │ O │ P │ Ü │ + │     │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┐    │
 * │      │ A │ S │ D │ F │ G │ H │ J │ K │ L │ Ö │ Ä │ # │    │
 * ├────┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┴────┤
 * │    │ < │ Y │ X │ C │ V │ B │ N │ M │ , │ . │ - │          │
 * ├────┼───┴┬──┴─┬─┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤
 * │    │    │    │                        │    │    │    │    │
 * └────┴────┴────┴────────────────────────┴────┴────┴────┴────┘
 * Shifted symbols
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐
 * │ ° │ ! │ " │ § │ $ │ % │ & │ / │ ( │ ) │ = │ ? │ ` │       │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤
 * │     │   │   │   │   │   │   │   │   │   │   │   │ * │     │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┐    │
 * │      │   │   │   │   │   │   │   │   │   │   │   │ ' │    │
 * ├────┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┴────┤
 * │    │ > │   │   │   │   │   │   │   │ ; │ : │ _ │          │
 * ├────┼───┴┬──┴─┬─┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤
 * │    │    │    │                        │    │    │    │    │
 * └────┴────┴────┴────────────────────────┴────┴────┴────┴────┘
 *  AltGr symbols
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐
 * │   │   │ ² │ ³ │   │   │   │ { │ [ │ ] │ } │ \ │   │       │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤
 * │     │ @ │   │ € │   │   │   │   │   │   │   │   │ ~ │     │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┐    │
 * │      │   │   │   │   │   │   │   │   │   │   │   │   │    │
 * ├────┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┴────┤
 * │    │ | │   │   │   │   │   │   │ µ │   │   │   │          │
 * ├────┼───┴┬──┴─┬─┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤
 * │    │    │    │                        │    │    │    │    │
 * └────┴────┴────┴────────────────────────┴────┴────┴────┴────┘
 */

/* Normal Symbols */
// Row 1
#define DE_CIRC 53 	// ^  and °
#define DE_1	30      // 1 and !
#define DE_2    31    	// 2 and " and ²
#define DE_3    32    	// 3 and § and ³
#define DE_4    33    	// 4 and $
#define DE_5    34   	// 5 and %
#define DE_6    35   	// 6 and &
#define DE_7    36    	// 7 and / and {
#define DE_8    37    	// 8 and ( and [
#define DE_9    38   	// 9 and ) and ]
#define DE_0    39   	// 0 and = and }
#define DE_SS   45 	// ß and ? and '\'
#define DE_ACUT 46 	// ´ and `
// Row 2
#define DE_Q    20    	// q and Q and @
#define DE_W    26    	// w and W
#define DE_E    8     	// e and E and €
#define DE_R    21    	// r and R and
#define DE_T    23    	// t and T
#define DE_Z    28    	// z and Z
#define DE_U    24    	// u and U
#define DE_I    12    	// i and I
#define DE_O    18    	// o and O
#define DE_P    19    	// p and P
#define DE_UDIA 47	// ü and Ü
#define DE_PLUS 48	// + and * and ~
// Row 3
#define DE_A    4	// a and A
#define DE_S    22    	// s and S
#define DE_D    7    	// d and D
#define DE_F    9    	// f and F
#define DE_G    10   	// g and G
#define DE_H    11    	// h and H
#define DE_J    13    	// j and  J
#define DE_K    14    	// k and K
#define DE_L    15    	// l and L
#define DE_ODIA 51	// ö and Ö
#define DE_ADIA 52	// ä and Ä
#define DE_HASH	50  	// # and '
// Row 4
#define DE_LABK 100   	// < and > and |
#define DE_Y    29    	// y and Y
#define DE_X    27    	// x and X
#define DE_C    6    	// c and C
#define DE_V    25    	// v and V
#define DE_B    5   	// b and B
#define DE_N    17   	// n and N
#define DE_M    16   	// m and M and µ
#define DE_COMM 54	// , and ;
#define DE_DOT  55	// . and :
#define DE_MINS 56	// - and _

// Extra and Extended signs
#define DE_RET 		40 	// return (Enter)
#define DE_ESC 		41 	// ESC
#define DE_BS  		42 	// backspace
#define DE_TAB		43	// TAB
#define DE_SPACE	44	// space
#define DE_NUM		83	// num block on/off

// signs F1 to F12
#define F1	58
#define F2	59
#define F3	60
#define F4	61
#define F5	62
#define F6	63
#define F7	64
#define F8	65
#define F9	66
#define F10	67
#define F11	68
#define F12	69

#endif /* INC_USB_HID_KEYS_GERMAN_H_ */
