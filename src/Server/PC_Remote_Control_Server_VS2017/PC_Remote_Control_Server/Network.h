/* This file is Coded in Pure C code, nothing in here ist coded in C++, expect from the
 * #pragma once compiler directive to make sure, this header is only included once in the build
 */
#pragma once
#include <stdint.h>

#define BUFFERSIZE 4
constexpr auto KEYBOARD_USB_REPORT_SIZE = 8;
constexpr auto MOUSE_USB_REPORT_SIZE = 4;

void receive_udp_datagram(void);
void udp_send_init(void);
void send_udp_datagram_USB_keyboard(unsigned char* ciphertext, int bytecount);
//void send_udp_datagram_USB_mouse(uint16_t mouseReport[MOUSE_USB_REPORT_SIZE]);
void send_udp_datagram_USB_mouse(signed char mouseReport[MOUSE_USB_REPORT_SIZE]);
//void send_udp_datagram(char* input);
void udp_close_socket(void);

