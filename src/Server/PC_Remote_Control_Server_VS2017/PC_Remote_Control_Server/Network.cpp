﻿/* This file is Coded in Pure C code, nothing in here ist coded in C++, expect from the 
 * #pragma comment() compiler directive to link the Winsock library. 
 */

#include <winsock.h>
#include <string>
#include "Network.h"
#include "Engine.h"
#include "tweetnacl.h"

#pragma comment(lib,"Ws2_32.lib")

#define SERVERPORT 8080
#define CLIENTPORT 8080

int clearWinSock(void) {
	int res = WSACleanup();
	return res;
}

std::string outmsg;

unsigned char buffer[BUFFERSIZE]; //buffer where the received datagram ist stored, unsigned oder uint8_t wichtig
void receive_udp_datagram(void) {
	outmsg = "____Receive client IP adress____\n";
	OutputDebugStringA(outmsg.c_str());
	// Step 1: Initialization of Winsock
	WSADATA winSockData;
	int wsaStartup;
	wsaStartup = WSAStartup(MAKEWORD(2, 2), &winSockData);
	if (wsaStartup != 0) {
		outmsg = "WSAStartup failed\n";
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "WSAStartup Success\n";
		OutputDebugStringA(outmsg.c_str());
	}

	// Step 2: Fill the udpClient (SOCKET ADDRESS) structure
	struct sockaddr_in udpClient;
	udpClient.sin_family = AF_INET;
	udpClient.sin_addr.s_addr = INADDR_ANY; // Client IP
	udpClient.sin_port = htons(SERVERPORT); // destination port where the datagram is received

	// Step 3: Create a socket for this Server
	SOCKET udpSocketServer;
	udpSocketServer = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);    //UDP Socket
	if (udpSocketServer == SOCKET_ERROR) {
		outmsg = "Socket creation failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "Socket successfully created\n";
		OutputDebugStringA(outmsg.c_str());
	}

	// Step 4: Bind the server socket
	int bindSocket = bind(udpSocketServer, (SOCKADDR*)&udpClient, sizeof(udpClient));
	if (bindSocket == SOCKET_ERROR) {
		outmsg = "Binding socket failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "Socket successfully binded\n\n";
		OutputDebugStringA(outmsg.c_str());
	}

	// Step 5: Receive data from client
	int recvData;
	recvData = recvfrom(udpSocketServer, (char*)buffer, sizeof(buffer), MSG_PEEK, NULL, NULL);
	if (recvData == SOCKET_ERROR) {
		outmsg = "Receiving datagram failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "Received data ->";
		outmsg += std::to_string(buffer[0]);
		outmsg += "\n";
		outmsg += "Received data ->";
		outmsg += std::to_string(buffer[1]);
		outmsg += "\n";
		outmsg = "Received data ->";
		outmsg += std::to_string(buffer[2]);
		outmsg += "\n";
		outmsg = "Received data ->";
		outmsg += std::to_string(buffer[3]);
		outmsg += "\n";
		OutputDebugStringA(outmsg.c_str());
	}

	// Step 6: Close socket
	int closeSocket;
	closeSocket = closesocket(udpSocketServer);
	if (closeSocket == SOCKET_ERROR) {
		outmsg = "Closing socket failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "Socket successfully closed\n";
		OutputDebugStringA(outmsg.c_str());
	}

	// Step 7: WSACleanup for terminating from DLL
	int wsaCleanup;
	wsaCleanup = clearWinSock();
	if (wsaCleanup == SOCKET_ERROR) {
		outmsg = "WSACleanup failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "WSACleanup Success\n";
		OutputDebugStringA(outmsg.c_str());
	}
}

struct sockaddr_in udpClient;
SOCKET udpSocketServer;

void udp_send_init(void) {
	outmsg = "\n____Send UPD datagram to client____\n";
	OutputDebugStringA(outmsg.c_str());
	// Step 1: Initialization of Winsock
	WSADATA winSockData;
	int wsaStartup;
	wsaStartup = WSAStartup(MAKEWORD(2, 2), &winSockData);
	if (wsaStartup != 0) {
		outmsg = "WSAStartup failed\n";
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "WSAStartup Success\n";
		OutputDebugStringA(outmsg.c_str());
	}


	// Step 2: Fill the udpServer (SOCKET ADDRESS) structure

	// Step 2.1: Get the correct format for the client IP adress to fill it into the udpClient Structure
	char bufferForIpAddr[12 + 3 + 1]; // 12 numbers + 3 dots + 1 termination zero

	sprintf_s(bufferForIpAddr, sizeof(bufferForIpAddr), "%d.%d.%d.%d", buffer[0], buffer[1], buffer[2], buffer[3]);

	// Step 2.2: Fill the structure
	udpClient.sin_family = AF_INET;
	udpClient.sin_addr.s_addr = inet_addr(bufferForIpAddr); // Client IP
	udpClient.sin_port = htons(CLIENTPORT); // Client port where the datagram gets send to

	// Step 3: Create a socket for this Server
	udpSocketServer = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);    //UDP Socket
	if (udpSocketServer == SOCKET_ERROR) {
		outmsg = "Socket creation failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "Socket successfully created\n";
		OutputDebugStringA(outmsg.c_str());
	}
}

// function for sending an decrypted usb keyboard report and a nonce to the µC
void send_udp_datagram_USB_keyboard(unsigned char* ciphertext, int bytecount) {
	// Step 4: send UDP datagram to client
	int sendData;
	// ACHTUNG! Daten sind unsigned werden aber als Signed verschickt, müssen daher beim Empfänger richtig interpretiert werden
	sendData = sendto(udpSocketServer, (char*)ciphertext, bytecount, MSG_DONTROUTE, (SOCKADDR*)&udpClient, sizeof(udpClient));
	if (sendData == SOCKET_ERROR) {
		outmsg = "Sending datagram failed\n";
		OutputDebugStringA(outmsg.c_str());
	}
	else {
	
		/*
		unsigned char* encryptedMessage = (unsigned char *)calloc(100, sizeof(int));
		for (int i = 0; i < bytecount; ++i) {
			encryptedMessage[i] = ciphertext[i];
		}

		char out[1000];
		
		sprintf_s(out, "ciphertext from report send: %s\n", encryptedMessage);
		OutputDebugStringA(out);

		unsigned char nonce[crypto_secretbox_NONCEBYTES];
		for (int i = bytecount; i > (bytecount - crypto_secretbox_NONCEBYTES); --i) {
			nonce[i - bytecount] = ciphertext[i];
		}
		sprintf_s(out, "nonce send: %s\n", nonce);
		OutputDebugStringA(out);
		

		sprintf_s(out, "complete message send: %s\n", ciphertext);
		OutputDebugStringA(out);
		*/
		outmsg = "Datagram successfully sended\n";
		OutputDebugStringA(outmsg.c_str());
	}
}


/*
// function for sending an usb keyboard report
void send_udp_datagram_USB_keyboard(unsigned char keyboardReport[KEYBOARD_USB_REPORT_SIZE + 1]) {
	// Step 4: send UDP datagram to client
	int sendData;
	// ACHTUNG! Daten sind unsigned werden aber als Signed verschickt, müssen daher beim Empfänger richtig interpretiert werden
	sendData = sendto(udpSocketServer, (char*)keyboardReport, sizeof(keyboardReport[0]) * (KEYBOARD_USB_REPORT_SIZE + 1), MSG_DONTROUTE, (SOCKADDR*)&udpClient, sizeof(udpClient));
	if (sendData == SOCKET_ERROR) {
		outmsg = "Sending datagram failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "Datagram successfully sended\n";
		OutputDebugStringA(outmsg.c_str());
	}
}
*/

// send function for usb mouse report
//void send_udp_datagram_USB_mouse(uint16_t mouseReport[MOUSE_USB_REPORT_SIZE]) {
	void send_udp_datagram_USB_mouse(signed char mouseReport[MOUSE_USB_REPORT_SIZE]) {
	// Step 4: send UDP datagram to client
	int sendData;
	sendData = sendto(udpSocketServer, (char*)mouseReport, sizeof(mouseReport[0]) * MOUSE_USB_REPORT_SIZE, MSG_DONTROUTE, (SOCKADDR*)&udpClient, sizeof(udpClient));
	if (sendData == SOCKET_ERROR) {
		outmsg = "Sending datagram failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	/*
	else {
		outmsg = "Datagram successfully sended\n";
		OutputDebugStringA(outmsg.c_str());
	}
	*/
}

/*
// generic send function for sending any strings
void send_udp_datagram(char *input) {
	// Step 4: send UDP datagram to client
	std::string outmsg1 = input;
	OutputDebugStringA(outmsg1.c_str());
	int sendData;
	sendData = sendto(udpSocketServer, input, strlen(input) + 1, MSG_DONTROUTE, (SOCKADDR*)&udpClient, sizeof(udpClient));
	if (sendData == SOCKET_ERROR) {
		outmsg = "Sending datagram failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "Datagram successfully sended\n";
		OutputDebugStringA(outmsg.c_str());
	}
}
*/

void udp_close_socket(void) {
	// Step 5: Close socket
	int closeSocket;
	closeSocket = closesocket(udpSocketServer);
	if (closeSocket == SOCKET_ERROR) {
		outmsg = "Closing socket failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "Socket successfully closed\n";
		OutputDebugStringA(outmsg.c_str());
	}

	// Step 6: WSACleanup for terminating from DLL
	int wsaCleanup;
	wsaCleanup = clearWinSock();
	if (wsaCleanup == SOCKET_ERROR) {
		outmsg = "WSACleanup failed -> %d\n", WSAGetLastError();
		OutputDebugStringA(outmsg.c_str());
	}
	else {
		outmsg = "WSACleanup Success\n";
		OutputDebugStringA(outmsg.c_str());
	}
}

