#pragma once

constexpr auto notFound = -999;
constexpr auto USB_CTRL = 0x01;
constexpr auto USB_SHIFT = 0x02;
constexpr auto USB_CTRL_ALT = -0x40;

class Hashmap
{
public:
	void FillHashmap_Keys();
	int FindHIDKey(int InputKey);
};