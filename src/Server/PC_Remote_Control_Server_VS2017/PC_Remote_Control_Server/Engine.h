#pragma once
#include "WindowContainer.h"
#include "Hashmap.h"
#include "Network.h"

#define KEYBOARDREPORT_SIZE 9
#define crypto_secretbox_MACBYTES 16

class Engine : WindowContainer
{
public:
	unsigned char keyboardReport[KEYBOARD_USB_REPORT_SIZE + 1];
	signed char mouseReport[MOUSE_USB_REPORT_SIZE];
	void StartNetwork();
	Hashmap Hashmap;
	bool Initialize(HINSTANCE hInstance, std::string window_title, std::string window_class, int width, int height, bool useNetwork = false);
	bool ProcessMessages();
	bool isMod(int KeyAsHID);
	void Update();
	void Test();

	void noncegen();
	void nonceinc(unsigned char *nonce);
	void encrypt();
	void decrypt();

private:
	bool useNetwork;
	void HashmapInit();
};