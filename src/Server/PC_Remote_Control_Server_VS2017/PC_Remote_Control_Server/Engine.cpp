/* -	This Project is adoptet from https://github.com/Pindrought/DirectX-11-Engine-VS2017/tree/Tutorial_6
 *		and only adaptet by me for usage.
 * -	Most Functions used for the Cryoptographie are adoptet from https://gist.github.com/augustoroman/9066d893ab7604dc73fc38e3c860612d
 * 
 * -	tweetnacl.c and tweetnacl.h are the most important functions of the NaCl library for use in any C Programm https://tweetnacl.cr.yp.to/
 */
#include "Engine.h"
#include "tweetnacl.h"
#include "profileapi.h"

// tpydef for a message or string
typedef struct {
	unsigned char* data;
	int size;
} Message;

//  Variables
Message m;
const char* HEX_KEY = "6f763ce9f29b0bc48d662188debc897945df779f68b532440d8e6fffeb2e54bc";
const char * HEX_NONCE = "2923be84e16cd6ae529049f1f1bbe9ebb3a6db3c870c3e99";
Message n; // nonce
unsigned char n_buff[crypto_secretbox_NONCEBYTES];
Message k; // key
// for encryption
Message m_padded;
unsigned char m_paddedBuff[200];
Message c;
unsigned char cBuff[200];
Message c_trimmed;
// for decryption
Message c_padded;
Message m_out;
Message m_out_trimmed;
// for sending and reveiving messages
Message outMsg; //[c_trimmed.size + crypto_secretbox_NONCEBYTES];
Message inMsg; // [clen + crypto_secretbox_NONCEBYTES];
int trimmed_len;
int len;
char out[10000];
unsigned char buff_empty[10000];
unsigned char kBuff[200];
unsigned char nBuff[200];
unsigned char testReport[KEYBOARDREPORT_SIZE];

static inline int64_t GetTicks()
{
	LARGE_INTEGER ticks;
	if (!QueryPerformanceCounter(&ticks))
	{
		printf("kaputt\n"); while(1);
	}
	return ticks.QuadPart;
}

// function for creating a message
Message fromStr(const char* str) {
	Message m = { (unsigned char*)str, strlen(str) };
	return m;
}

// function for creating an empty message
Message newEmpty(int len, unsigned char * buff) {		// buffer bei �C als Parameter
	Message m = { (unsigned char*) buff, len };
	memset(m.data, 0, len);
	return m;
}

// needed padding for encrypting and decrypting a message with the nacl C interface
Message zeroPadPrefix(Message m, int len, unsigned char * buff) {
	Message m2 = newEmpty(m.size + len, buff);
	memcpy(m2.data + len, m.data, m.size);
	return m2;
}

// random function to generate a random nonce +		n	{data=0x00007ff64ad5eb30 "" size=24 }	Message
void __cdecl randombytes(unsigned char * dst, unsigned long long count) {
	long long t = GetTicks();
	for (unsigned long long i = 0; i < count; ++i) {
		*(dst + i) = (unsigned char) t;
		t = GetTicks();
	}
}


// Convert string of chars to its NULL-terminated representative string of hex
// numbers. The returned pointer must be freed by the caller.
const char* string2hex(Message str, int capital)
{
	int hexstr_size = str.size * 2;
	char* hexstr = (char *)calloc(hexstr_size + 1, 1); // +1 for terminating NULL

	const int a = capital ? 'A' - 1 : 'a' - 1;

	for (int i = 0, c = str.data[0] & 0xFF; i < hexstr_size; c = str.data[i / 2] & 0xFF)
	{
		hexstr[i++] = c > 0x9F ? (c / 16 - 9) | a : c / 16 | '0';
		hexstr[i++] = (c & 0xF) > 9 ? (c % 16 - 9) | a : c % 16 | '0';
	}

	return hexstr;
}

// Convert string of hex numbers to its equivalent char-stream
Message hex2string(const char* hexstr, unsigned char *buff)
{
	int size = strlen(hexstr);
	Message str = newEmpty((size + 1) / 2, buff);

	for (int i = 0, j = 0; i < str.size; i++, j++)
	{
		str.data[i] = (hexstr[j] & '@' ? hexstr[j] + 9 : hexstr[j]) << 4, j++;
		str.data[i] |= (hexstr[j] & '@' ? hexstr[j] + 9 : hexstr[j]) & 0xF;
	}
	return str;
}

void Engine::noncegen() {
	k = hex2string(HEX_KEY, kBuff);
	char out[500];
	sprintf_s(out, "\nkey: %s\n", k.data);
	OutputDebugStringA(out);

	n.data = n_buff;
	n.size = crypto_secretbox_NONCEBYTES; 
	randombytes(n.data, crypto_secretbox_NONCEBYTES);
	//n = hex2string(HEX_NONCE, nBuff);

	sprintf_s(out, "Nonce: %s\n\n", n.data);
	OutputDebugStringA(out);
}
// Funktion incrementNonce(unsigned char * nonce) addiere 1 auf *nonce, wert an der stelle = 0, dann das n�chste byte inkrementieren
void Engine::nonceinc(unsigned char *nonce) {
	int dst = 0;
	*(nonce + dst) += 1;
	while (*(nonce + dst) == 0 && dst < crypto_secretbox_NONCEBYTES) {
		++dst;
		*(nonce + dst) += 1;
	}
}

unsigned char mBuff[200];

void Engine::encrypt() {
	m = fromStr((char*)"Halloaa");
	m_padded = zeroPadPrefix(m, crypto_secretbox_ZEROBYTES, m_paddedBuff);
	c = newEmpty(m_padded.size, cBuff);

	int ret = crypto_secretbox(c.data, m_padded.data, m_padded.size, n.data, k.data);

	char out[500];
	sprintf_s(out, "Message: %s\n", m.data);
	OutputDebugStringA(out);

	c_trimmed = {
		c.data + crypto_secretbox_BOXZEROBYTES,
		c.size - crypto_secretbox_BOXZEROBYTES
	};

	sprintf_s(out, "Ciphertext: %s\n", c_trimmed.data);	OutputDebugStringA(out);
}

unsigned char c_paddedBuff[200];
unsigned char m_outBuff[200];
void Engine::decrypt() {
	c_padded = zeroPadPrefix(c_trimmed, crypto_secretbox_BOXZEROBYTES, c_paddedBuff);
	m_out = newEmpty(c_trimmed.size + crypto_secretbox_ZEROBYTES, m_outBuff);

	int retopen = crypto_secretbox_open(m_out.data, c_padded.data, c_padded.size, n.data, k.data);

	m_out_trimmed = {
		m_out.data + crypto_secretbox_ZEROBYTES,
		m_out.size - crypto_secretbox_ZEROBYTES
	};
	//m_out_trimmed.size = strnlen((const char*)m_out_trimmed.data, m_out_trimmed.size);
	char out[500];
	sprintf_s(out, "Decrypted Message: %s\n\n", m_out_trimmed.data);
	OutputDebugStringA(out);
}

void Engine::StartNetwork()
{
	receive_udp_datagram();
	udp_send_init();
}

void Engine::HashmapInit()
{
	Hashmap.FillHashmap_Keys();
}

bool Engine::Initialize(HINSTANCE hInstance, std::string window_title, std::string window_class, int width, int height, bool useNetwork)
{
	this->HashmapInit();
	this->mouseReport[0] = 2;
	this->keyboardReport[0] = 1;
	this->keyboardReport[8] = 1;

	this->useNetwork = useNetwork;
	if (useNetwork)
	{
		this->StartNetwork();
	}
	return this->render_window.Initialize(this, hInstance, window_title, window_class, width, height);
}

bool Engine::ProcessMessages()
{
	return this->render_window.ProcessMessages();
}

bool Engine::isMod(int KeyAsHID) {
	if (KeyAsHID == USB_CTRL || KeyAsHID == USB_SHIFT || KeyAsHID == USB_CTRL_ALT)
	{
		return true;
	}
	return false;
}


// Testfunktion
std::string outcounter;
unsigned int counter = 0;
void Engine::Test() {

	this->keyboardReport[3] = (counter % 10) + 30;
	m = fromStr((char*)this->keyboardReport);
	m.size = KEYBOARDREPORT_SIZE;
	m_padded = zeroPadPrefix(m, crypto_secretbox_ZEROBYTES, m_paddedBuff);
	c = newEmpty(m_padded.size, cBuff);
	int ret = crypto_secretbox(c.data, m_padded.data, m_padded.size, n.data, k.data);
	c_trimmed = {
		c.data + crypto_secretbox_BOXZEROBYTES,
		c.size - crypto_secretbox_BOXZEROBYTES
	};
	trimmed_len = c_trimmed.size;
	outMsg = fromStr((char*)c_trimmed.data);
	outMsg.size += crypto_secretbox_NONCEBYTES;
	memcpy(outMsg.data + c_trimmed.size, n.data, crypto_secretbox_NONCEBYTES);
	len = trimmed_len + crypto_secretbox_NONCEBYTES;
	send_udp_datagram_USB_keyboard(outMsg.data, len);
	nonceinc(n.data);

	//Sleep(10); //sleep

	outcounter = std::to_string(this->keyboardReport[3]);
	outcounter += "\n";
	OutputDebugStringA(outcounter.c_str());
	++counter;

	this->keyboardReport[3] = 0;
	m = fromStr((char*)this->keyboardReport);
	m.size = KEYBOARDREPORT_SIZE;
	m_padded = zeroPadPrefix(m, crypto_secretbox_ZEROBYTES, m_paddedBuff);
	c = newEmpty(m_padded.size, cBuff);
	int retTest = crypto_secretbox(c.data, m_padded.data, m_padded.size, n.data, k.data);
	c_trimmed = {
		c.data + crypto_secretbox_BOXZEROBYTES,
		c.size - crypto_secretbox_BOXZEROBYTES
	};
	trimmed_len = c_trimmed.size;
	outMsg = fromStr((char*)c_trimmed.data);
	outMsg.size += crypto_secretbox_NONCEBYTES;
	memcpy(outMsg.data + c_trimmed.size, n.data, crypto_secretbox_NONCEBYTES);
	len = trimmed_len + crypto_secretbox_NONCEBYTES;
	send_udp_datagram_USB_keyboard(outMsg.data, len);
	nonceinc(n.data);

	//Sleep(10); // sleep
}


// handle when two press events occure and one is released
// int prevPressed = 0;
void Engine::Update()
{
	// Keyboard Events
	while (!keyboard.KeyBufferIsEmpty())
	{
		KeyboardEvent kbe = keyboard.ReadKey();
		unsigned char keycode = kbe.GetKeyCode();

		int KeyAsHID = Hashmap.FindHIDKey(keycode);

		std::string keyEvent;
		std::string HIDKey;

		// print convertet HID key or modifier
		if (kbe.IsPress())
		{
			keyEvent = "\nKey Press\n";
			if (KeyAsHID != notFound)
			{
				if (isMod(KeyAsHID))
				{
					HIDKey = "HIDModifier: ";
					this->keyboardReport[1] = abs(KeyAsHID);
				}
				else
				{
					HIDKey = "HIDKey: ";
					for (int i = 3; i <= 7; ++i)
					{
						if (this->keyboardReport[i] == 0)
						{
							this->keyboardReport[i] = KeyAsHID;
							break;
						}
					}
				}
			}
		}
		if (kbe.IsRelease())
		{
			keyEvent = "\nKey Release\n";
			if (isMod(KeyAsHID))
			{
				HIDKey = "HIDModifier: ";
				this->keyboardReport[1] = 0;
			}
			else
			{
				HIDKey = "HIDKey: ";
				for (int i = 3; i <= 7; ++i) {
					if (this->keyboardReport[i] == KeyAsHID) {
						this->keyboardReport[i] = 0;
					}
				}
			}
		}
		OutputDebugStringA(keyEvent.c_str());

		// print keycode macro
		std::string outmsg = "DirectInput Keycode as Macro: ";
		outmsg += keycode;
		outmsg += "\n";
		OutputDebugStringA(outmsg.c_str());

		// print keycode as int
		unsigned int keyCodeInt = kbe.GetKeyCode();
		std::string outmsgChar = "DirectInput Keycode as int: ";
		outmsgChar += std::to_string(keyCodeInt);
		outmsgChar += "\n";
		OutputDebugStringA(outmsgChar.c_str());

		HIDKey += std::to_string(KeyAsHID);
		HIDKey += "\n";
		OutputDebugStringA(HIDKey.c_str());

		// print report to send
		std::string outreport = "Values of Report: report[0]: ";
		outreport += std::to_string(keyboardReport[0]);
		outreport += ", report[1]: ";
		outreport += std::to_string(keyboardReport[1]);
		outreport += ", report[3]: ";
		outreport += std::to_string(keyboardReport[3]);
		outreport += ", report[4]: ";
		outreport += std::to_string(keyboardReport[4]);
		outreport += ", report[5]: ";
		outreport += std::to_string(keyboardReport[5]);
		outreport += ", report[8]: ";
		outreport += std::to_string(keyboardReport[8]);
		outreport += "\n";
		OutputDebugStringA(outreport.c_str());

		if (this->useNetwork)
		{
			/*
			// Ausgabe 
			Message nInHex = fromStr((char*)string2hex(n, 0));
			sprintf_s(out, "\n\nNonce in Hex: %s\n", nInHex.data);
			OutputDebugStringA(out);

			sprintf_s(out, "Nonce in Str: %s\n", n.data);
			OutputDebugStringA(out);
			*/
			
			//m = fromStr((char*)this->keyboardReport);
			m = fromStr((char*)this->keyboardReport);
			m.size = KEYBOARDREPORT_SIZE;
			

			/*
			// Ausgabe
			for (int i = 0; i < m.size; ++i) {
				sprintf_s(out, "Report[%d]: %d\n", i, m.data[i]);
				OutputDebugStringA(out);
			}
			sprintf_s(out, "Message: %s\n", m.data);
			OutputDebugStringA(out);
			*/
			
			m_padded = zeroPadPrefix(m, crypto_secretbox_ZEROBYTES, m_paddedBuff);
	
			c = newEmpty(m_padded.size, cBuff);
			
			int ret = crypto_secretbox(c.data, m_padded.data, m_padded.size, n.data, k.data);

			c_trimmed = {
				c.data + crypto_secretbox_BOXZEROBYTES,
				c.size - crypto_secretbox_BOXZEROBYTES
			};
			trimmed_len = c_trimmed.size;
			/*
			// Ausgabe
			sprintf_s(out, "Ciphertext in Hex: %s\n", string2hex(c_trimmed, 0));
			OutputDebugStringA(out);
			sprintf_s(out, "Ciphertext in Str: %s\n", c_trimmed.data);
			OutputDebugStringA(out);
			*/

			// copy ciphertext in outMsg
			outMsg = fromStr((char*)c_trimmed.data);
			outMsg.size += crypto_secretbox_NONCEBYTES;

			/*
			// Ausgabe
			sprintf_s(out, "ciphertext in outMsg in Hex: %s\n", string2hex(outMsg, 0));
			OutputDebugStringA(out);
			sprintf_s(out, "Ciphertext in outMsg in Str: %s\n", outMsg.data);
			OutputDebugStringA(out);
			*/
		
			// copy nonce behind ciphertext in outMsg 
			memcpy(outMsg.data + c_trimmed.size, n.data, crypto_secretbox_NONCEBYTES);
			
			/*
			// Ausgabe 
			unsigned char nInOutMsg[crypto_secretbox_NONCEBYTES];
			memcpy(nInOutMsg, outMsg.data + c_trimmed.size, crypto_secretbox_NONCEBYTES);

			sprintf_s(out, "Nonce in outMsg in Str: %s", nInOutMsg);
			out[100] = '\0';
			OutputDebugStringA(out);
			
			Message nInOutMsg2Hex = fromStr((char *)nInOutMsg);
			sprintf_s(out, "\nNonce in outMsg in Hex: %s", string2hex(nInOutMsg2Hex, 0));
			out[100] = '\0';
			OutputDebugStringA(out);

			sprintf_s(out, "\ncomplete outMsg: %s\n", outMsg.data);
			out[100] = '\0';
			OutputDebugStringA(out); 
			*/

			len = trimmed_len + crypto_secretbox_NONCEBYTES;

			send_udp_datagram_USB_keyboard(outMsg.data, len);
			nonceinc(n.data);

			Sleep(15);
		}
	}
}

// Mouse Events
/*
while (!mouse.EventBufferIsEmpty())
{
	MouseEvent me = mouse.ReadEvent();

	if (me.GetType() == MouseEvent::EventType::RAW_MOVE) {
		mouseReport[2] = me.GetPosX() * 5;
		mouseReport[3] = me.GetPosY() * 5;
	}

	// mouse press events
	if (me.GetType() == me.LPress) {
		// prevPressed = mouseReport[1];
		mouseReport[1] = 4;
		keyboardReport[1] = 4;
	}
	if (me.GetType() == me.RPress) {
		// prevPressed = mouseReport[1];
		mouseReport[1] = 1;
		keyboardReport[1] = 1;
	}
	if (me.GetType() == me.MPress) {
		// prevPressed = mouseReport[1];
		mouseReport[1] = 2;
		keyboardReport[1] = 2;
	}

	// mouse release events
	if (me.GetType() == me.LRelease) {
		if (mouseReport[1] == 4) {
			mouseReport[1] = 0;
			keyboardReport[1] = 0;
		}
	}
	if (me.GetType() == me.RRelease) {
		if (mouseReport[1] == 1) {
			mouseReport[1] = 0;
			keyboardReport[1] = 0;
		}
	}
	if (me.GetType() == me.MRelease) {
		if (mouseReport[1] == 2) {
			mouseReport[1] = 0;
			keyboardReport[1] = 0;
		}
	}

	// print report to send
	std::string outreport = "Values of Report: report[0]: ";
	outreport += std::to_string(mouseReport[0]);
	outreport += ", report[1]: ";
	outreport += std::to_string(mouseReport[1]);
	outreport += ", report[2]: ";
	outreport += std::to_string(mouseReport[2]);
	outreport += ", report[3]: ";
	outreport += std::to_string(mouseReport[3]);
	outreport += ", report[4]: ";
	outreport += std::to_string(mouseReport[4]);
	outreport += "\n";
	OutputDebugStringA(outreport.c_str());

	// send report
	if (this->useNetwork)
	{
		send_udp_datagram_USB_mouse(this->mouseReport);
	}
}
*/