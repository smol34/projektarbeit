/* Wichtig: Das Projekt wurde in gro�en Teilen von https://github.com/Pindrought/DirectX-11-Engine-VS2017/tree/Tutorial_6 �bernommen. 
			Klassen wurden f�r die Nutzung in diesem Projekt angepasst und die Network Klassen wurden selbst erstellt.
*/   

#include "Engine.h"

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow) {
/* 	Keyboard report structure:
	unsigned char report[9]
	report[0] = 1  // reportID
	report[1] = 0; // modifier
	report[2] = 0; // OEM
	report[3] = 0; // keycode data
	report[4] = 0; // keycode data
	report[5] = 0; // keycode data
	report[6] = 0; // keycode data
	report[7] = 0; // keycode data
	report[8] = 1; // tell the �Controller the C++ program is the src

	Mouse report Structure:
	signed char report[5];
	report[0] = 2  // reportID
	report[1] = 0; // button status -> 1 == Right Button, 2 == Middle Button, 4 == Left Button
	report[2] = 0; // X axis
	report[3] = 0; // y axis
	report[4] = 0; // wheel
*/


	Engine engine;

	// Absicherung mit kryptographischer Bibliothek NaCl

	engine.noncegen();
	engine.encrypt();
	engine.decrypt();

	// �bertragen der Daten
	engine.Initialize(hInstance, "PC_Remote_Control_Server", "MyWindowClass", 800, 600, true);
	
	
	while (engine.ProcessMessages() == true)
	{
		engine.Update();
	} 
	
	
	// Nicht nach jedem Test wurde eine Anzahl der erhaltenen Pakete angezeigt
	/*
	for (int i = 0; i < 100; ++i) {
		engine.Test();	
	}			
	return 0;
	*/
}