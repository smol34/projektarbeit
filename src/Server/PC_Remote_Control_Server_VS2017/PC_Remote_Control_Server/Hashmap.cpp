#include "Hashmap.h"
#include <map>
#include <Windows.h>
#include <WinUser.h>
//#include <iostream> 
//#include <iterator> 

//  Key ist the DirectInput Keyocde and the value is the USB HID Keycode
std::map<int, int> KeycodeMap;	// Empty map container for keys and modifiers
std::map<int, int>::iterator it;

// insert all Keycodes
void Hashmap::FillHashmap_Keys()
{
	// modifiers
	KeycodeMap.insert(std::pair<int, int>(VK_SHIFT, USB_SHIFT));
	KeycodeMap.insert(std::pair<int, int>(VK_CONTROL, USB_CTRL));
	KeycodeMap.insert(std::pair<int, int>(VK_MENU, USB_CTRL_ALT));
		

	// letters
	KeycodeMap.insert(std::pair<int, int>(89, 29));
	KeycodeMap.insert(std::pair<int, int>(90, 28));
	for (int i = 0; i < 25; ++i)
	{
		KeycodeMap.insert(std::pair<int, int>(i + 65, i + 4));
	}
	// numbers
	KeycodeMap.insert(std::pair<int, int>(48, 39));
	for (int i = 0; i < 9; ++i)
	{
		KeycodeMap.insert(std::pair<int, int>(i + 49, i + 30));
	}
	// num key numbers
	KeycodeMap.insert(std::pair<int, int>(96, 39));
	for (int i = 0; i < 9; ++i)
	{
		KeycodeMap.insert(std::pair<int, int>(i + 97, i + 30));
	}

	// F1 to F12
	for (int i = 0; i < 12; ++i)
	{
		KeycodeMap.insert(std::pair<int, int>(i + 112, i + 58));
	}

	// insert all the rest
	KeycodeMap.insert(std::pair<int, int>(VK_BACK, 42));
	KeycodeMap.insert(std::pair<int, int>(VK_TAB, 43));
	KeycodeMap.insert(std::pair<int, int>(VK_RETURN, 40));
	KeycodeMap.insert(std::pair<int, int>(VK_ESCAPE, 41));
	KeycodeMap.insert(std::pair<int, int>(VK_SPACE, 44));
	KeycodeMap.insert(std::pair<int, int>(VK_PRIOR, 75));
	KeycodeMap.insert(std::pair<int, int>(VK_NEXT, 78));
	KeycodeMap.insert(std::pair<int, int>(VK_END, 77));
	KeycodeMap.insert(std::pair<int, int>(VK_HOME, 74));
	KeycodeMap.insert(std::pair<int, int>(VK_LEFT, 80));
	KeycodeMap.insert(std::pair<int, int>(VK_UP, 82));
	KeycodeMap.insert(std::pair<int, int>(VK_RIGHT, 79));
	KeycodeMap.insert(std::pair<int, int>(VK_DOWN, 81));
	KeycodeMap.insert(std::pair<int, int>(VK_DELETE, 76));
	KeycodeMap.insert(std::pair<int, int>(VK_LWIN, 227));
	KeycodeMap.insert(std::pair<int, int>(VK_RWIN, 231));
	KeycodeMap.insert(std::pair<int, int>(VK_MULTIPLY, 85));
	KeycodeMap.insert(std::pair<int, int>(VK_ADD, 87));
	KeycodeMap.insert(std::pair<int, int>(VK_SUBTRACT, 86));
	KeycodeMap.insert(std::pair<int, int>(VK_DIVIDE, 84));
	// Makros passen hier nicht zum US Standard Tastaturlayout, sondern sind auf das Deutsche Layout angepasst
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_7, 52));		// � und �
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_1, 47));		// � und �
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_3, 51));		// � und �
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_5, 53));		// ^ und �
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_4, 45));		// � und ? und '\'
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_6, 46));		// �und `
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_PLUS, 48));	// + und * und ~
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_2, 50));		// # und '
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_MINUS, 56));	// - und _
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_COMMA, 54));	// , und ;
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_PERIOD, 55));	// . und :
	KeycodeMap.insert(std::pair<int, int>(VK_OEM_102, 100));	// < und > und |	
}

int Hashmap::FindHIDKey(int InputKey)
{
	it = KeycodeMap.find(InputKey);
	if (it == KeycodeMap.end())
	{
		return notFound;
	}
	return it->second;
}